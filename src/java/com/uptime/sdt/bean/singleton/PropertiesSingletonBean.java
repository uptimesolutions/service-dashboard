/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.sdt.bean.singleton;

import java.io.File;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author akumar
 */
public class PropertiesSingletonBean {
    private static final PropertiesSingletonBean INSTANCE = new PropertiesSingletonBean();
    private final Properties properties;
    private String env = null;
    
    
    /**
     * Private Constructor
     */
    private PropertiesSingletonBean() {
        properties = new Properties();
        env = null;
        
        try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties")) {
            properties.load(is);
            env = properties.getProperty("env");
            if(env == null || env.isEmpty())
                throw new Exception("Env variable missing");
        } catch(Exception ex) {
            Logger.getLogger(PropertiesSingletonBean.class.getName()).log(Level.SEVERE, "Fail to load properties file: ", ex);
        } 
    }

    /**
     * return a instance of the class 
     * @return PropertiesSingletonBean Object
     */
    public static PropertiesSingletonBean getInstance() {
        return INSTANCE;
    }
    
    /**
     * return the property based on the given key
     * @param key, String object
     * @return String Object
     */
    public String getProperty(String key) {
        return properties.getProperty(key + env);
    }
}
