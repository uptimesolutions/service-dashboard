/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.sdt.bean;

import com.uptime.sdt.bean.singleton.PropertiesSingletonBean;
import com.uptime.sdt.http.Client;
import com.uptime.sdt.vo.ActivityVO;
import com.uptime.sdt.vo.ServiceRegistrarVO;
import com.uptime.sdt.vo.ServiceVO;
import com.uptime.sdt.vo.SubscriptionVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author twilcox
 */
public class IndexBean implements Serializable {
    private List<ServiceRegistrarVO> serviceRegistrarList;
    private final Client CLIENT;
    private final String SERVICE_HOSTS_ENDPOINT, SUBSCRIPTIONS_ENDPOINT;
    private ActivityVO activity;
    private List<SubscriptionVO> subscriptions;
    private ServiceVO svoToBeShutdown, svoToBeRegistered;
    private ServiceRegistrarVO srvoToBeShutdown;
    private String registerIp, registerPort, currentService;
    
    /**
     * Constructor
     */
    public IndexBean() {
        serviceRegistrarList = new ArrayList();
        svoToBeRegistered = null;
        srvoToBeShutdown = null;
        svoToBeShutdown = null;
        CLIENT = new Client();
        
        SERVICE_HOSTS_ENDPOINT = "/system?serviceCommand=serviceHosts";
        SUBSCRIPTIONS_ENDPOINT = "/system?serviceCommand=subscriptions";
    }
    
    /**
     * Post Constructor
     */
    @PostConstruct
    private void init(){
        setServiceRegistrarList();
        serviceRegistrarList = CLIENT.getServiveHosts(serviceRegistrarList);
        serviceRegistrarList = CLIENT.getSubscriptions(serviceRegistrarList);
    }
    
    /**
     * Gets ServiceRegistrar urls from he property file
     * Sets the field serviceRegistrarList
     */
    private void setServiceRegistrarList() {
        String property;
        String[] hosts;
        ServiceRegistrarVO serviceRegistrarVO;
        
        try {
            if ((property = PropertiesSingletonBean.getInstance().getProperty("PEER_REGISTRAR_URL_LIST")) != null && !property.isEmpty()) {
                hosts = property.split(",");
                for (String host : hosts) {
                    if (host != null && !host.isEmpty()) {
                        serviceRegistrarVO = new ServiceRegistrarVO();
                        serviceRegistrarVO.setUrl(host.replace(" ", ""));
                        serviceRegistrarVO.setServiceHostsUrl(serviceRegistrarVO.getUrl() + SERVICE_HOSTS_ENDPOINT);
                        serviceRegistrarVO.setSubscriptionsUrl(serviceRegistrarVO.getUrl() + SUBSCRIPTIONS_ENDPOINT);
                        serviceRegistrarList.add(serviceRegistrarVO);
                    }
                }
            }
        } catch (Exception e) {  
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, "Exception in getServiceRegistrar: {0}", e.getMessage());
        }
    }
    
    /**
     * Returns the style for the given status
     * @param status, boolean, true if circuitBreakered else false
     * @return String object, of the needed style
     */
    public String statusColor(boolean status){
        return status ? "background-color:red;color:white;" : "background-color:green;color:white;";
    }

    /**
     * Gets a Service Host's info, from the given.
     * Checks if the service is registered or not
     */
    public void getServiceInfo(){
        RequestContext rc = RequestContext.getCurrentInstance();
        FacesContext fc = FacesContext.getCurrentInstance();
        boolean prexisting;
        
        try {
            svoToBeRegistered = CLIENT.getServiceInfo(registerIp, registerPort);
            
            prexisting = false;
            for(ServiceRegistrarVO registrar : serviceRegistrarList) {
                if(!prexisting && registrar.getSvoList() != null) {
                    for(ServiceVO service : registrar.getSvoList()) {
                        if (service.getIpAddress().equals(svoToBeRegistered.getIpAddress()) && service.getPort().equals(svoToBeRegistered.getPort())) {
                            prexisting = true;
                            break;
                        }
                    }
                }
            }
            
            if(prexisting) {
                fc.addMessage("growlMessages", new FacesMessage("The " + svoToBeRegistered.getName() + " host, already registered!"));
                svoToBeRegistered = null;
            } else
                rc.execute("PF('registerHostDlg').show()");
                
        } catch (Exception e) {
            svoToBeRegistered = null;
            Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Exception in getServiceInfo:{0}", e.getMessage());
            fc.addMessage("growlMessages", new FacesMessage("Registration Failed!"));
        }
    }
    
    /**
     * Checks the performs the given command 
     * @param svo, ServiceVO object
     */
    public void checkCommand(ServiceVO svo){
        RequestContext rc = RequestContext.getCurrentInstance();
        FacesContext fc = FacesContext.getCurrentInstance();
        
        switch(svo.getCommand().toLowerCase()){
            case "shutdown":
                svoToBeShutdown = svo;
                rc.execute("PF('confirmDlg').show()");
                break;
            case "unregister":
                try {
                    CLIENT.postSystemCommand(svo);
                    serviceRegistrarList = CLIENT.getServiveHosts(serviceRegistrarList);
                    serviceRegistrarList = CLIENT.getSubscriptions(serviceRegistrarList);
                    fc.addMessage("growlMessages", new FacesMessage("Unregister Succeeded!"));
                } catch (Exception e) {
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, "Exception in checkCommand:{0}", e.getMessage());
                    fc.addMessage("growlMessages", new FacesMessage("Unregister Failed!"));
                }
                break;
            case "unsubscribe":
                try {
                    CLIENT.postSystemCommand(svo);
                    serviceRegistrarList = CLIENT.getServiveHosts(serviceRegistrarList);
                    serviceRegistrarList = CLIENT.getSubscriptions(serviceRegistrarList);
                    fc.addMessage("growlMessages", new FacesMessage("Unsubscribe Succeeded!"));
                } catch (Exception e) {
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, "Exception in checkCommand:{0}", e.getMessage());
                    fc.addMessage("growlMessages", new FacesMessage("Unsubscribe Failed!"));
                }
                break;
            case "subscribe":
                try {
                    CLIENT.postSystemCommand(svo);
                    serviceRegistrarList = CLIENT.getServiveHosts(serviceRegistrarList);
                    serviceRegistrarList = CLIENT.getSubscriptions(serviceRegistrarList);
                    fc.addMessage("growlMessages", new FacesMessage("Subscribe Succeeded!"));
                } catch (Exception e) {
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, "Exception in checkCommand:{0}", e.getMessage());
                    fc.addMessage("growlMessages", new FacesMessage("Subscribe Failed!"));
                }
                break;
        }
    }
    
    public void unsubscribe(SubscriptionVO svo) {
        try {
            if(currentService != null && !currentService.isEmpty()) {
                CLIENT.putUnsubscribe(serviceRegistrarList, currentService, svo);
                serviceRegistrarList = CLIENT.getSubscriptions(serviceRegistrarList);
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Unsubscribe Succeeded!"));
            } else 
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Invalid Service!"));
        } catch (Exception e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, "Exception in unsubscribe:{0}", e.getMessage());
            FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Unsubscribe Failed!"));
        }
    }
    
    /**
     * Checks the performs the given command 
     * @param srvo, ServiceRegistrar object
     */
    public void checkCommand(ServiceRegistrarVO srvo){
        RequestContext rc = RequestContext.getCurrentInstance();
        
        switch(srvo.getCommand().toLowerCase()){
            case "shutdown":
                srvoToBeShutdown = srvo;
                rc.execute("PF('confirmDlg').show()");
                break;
        }
    }
    
    /**
     * Register a host
     */
    public void registerHost(){
        FacesContext content = FacesContext.getCurrentInstance();
        try {
            CLIENT.postSystemCommand(svoToBeRegistered );
            serviceRegistrarList = CLIENT.getServiveHosts(serviceRegistrarList);
            serviceRegistrarList = CLIENT.getSubscriptions(serviceRegistrarList);
            content.addMessage("growlMessages", new FacesMessage("Register Succeeded!"));
        } catch (Exception e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, "Exception in registerHost:{0}", e.getMessage());
            content.addMessage("growlMessages", new FacesMessage("Register Failed!"));
        }
        registerIp = "";
        registerPort = "";
        svoToBeRegistered = null;
    }
    
    /**
     * Cancels the Register command
     */
    public void cancelRegister(){
        registerIp = "";
        registerPort = "";
        svoToBeRegistered = null;
    }
    
    /**
     * Shutdown a host
     */
    public void shutdownHost(){
        FacesContext content = FacesContext.getCurrentInstance();
        
        try {
            if (svoToBeShutdown != null) {
                CLIENT.postSystemCommand(svoToBeShutdown);
                serviceRegistrarList = CLIENT.getServiveHosts(serviceRegistrarList);
                serviceRegistrarList = CLIENT.getSubscriptions(serviceRegistrarList);
                content.addMessage("growlMessages", new FacesMessage("Shutdown Succeeded!"));
            } else if (srvoToBeShutdown != null) {
                CLIENT.postSystemCommand(srvoToBeShutdown);
                serviceRegistrarList = CLIENT.getServiveHosts(serviceRegistrarList);
                serviceRegistrarList = CLIENT.getSubscriptions(serviceRegistrarList);
                content.addMessage("growlMessages", new FacesMessage("Shutdown Succeeded!"));
            }
        }catch (Exception e) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, "Exception in shutdownHost:{0}", e.getMessage());
            content.addMessage("growlMessages", new FacesMessage("Shutdown Failed!"));
        }
        svoToBeShutdown = null;
        srvoToBeShutdown = null;
    }
    
    /**
     * Cancels the Shutdown command
     */
    public void cancelShutdown(){
        svoToBeShutdown = null;
        srvoToBeShutdown = null;
    }

    /**
     * @return the serviceRegistrarList
     */
    public List<ServiceRegistrarVO> getServiceRegistrarList() {
        return serviceRegistrarList;
    }

    /**
     * @param serviceRegistrarList the serviceRegistrarList to set
     */
    public void setServiceRegistrarList(List<ServiceRegistrarVO> serviceRegistrarList) {
        this.serviceRegistrarList = serviceRegistrarList;
    }

    /**
     * @return the activity
     */
    public ActivityVO getActivity() {
        return activity;
    }

    /**
     * Set the field avo from the given ipAddress and port
     * @param ipAddress, String object
     * @param port, String object
     */
    public void setActivity(String ipAddress, String port) {
        ActivityVO avo;
        try {
            activity = CLIENT.getActivity(ipAddress, port);
            serviceRegistrarList.stream().filter(registrar -> registrar.getSvoList() != null && !registrar.getSvoList().isEmpty()).forEachOrdered(registrar -> {
                registrar.getSvoList().stream().filter(svo -> svo.getIpAddress().equals(ipAddress) && svo.getPort().equals(port)).forEachOrdered(svo -> svo.setConnection(""));
            });
            Logger.getLogger(Client.class.getName()).log(Level.INFO, "Activity obtained for Ip Address:{0} and Port:{1}", new Object[]{ipAddress, port});
        } catch (Exception e) {
            Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Exception in setActivity:{0}", e.getMessage());
            avo = new ActivityVO();
            avo.setCurrentRequests("null");
            avo.setFreeMemory("null");
            avo.setThreads("null");
            avo.setTotalMemory("null");
            activity = avo;
            serviceRegistrarList.stream().filter(registrar -> registrar.getSvoList() != null && !registrar.getSvoList().isEmpty()).forEachOrdered(registrar -> {
                registrar.getSvoList().stream().filter(svo -> svo.getIpAddress().equals(ipAddress) && svo.getPort().equals(port)).forEachOrdered(svo -> svo.setConnection("!"));
            });
        }
    }

    /**
     * Set the field avo from the given ipAddress and port
     * @param url, String object
     */
    public void setActivity(String url) {
        ActivityVO avo;
        try {
            activity = CLIENT.getActivity (url);
            serviceRegistrarList.stream().filter(s -> s.getUrl().equals(url)).forEach(svo -> svo.setBadConnection(false));
            Logger.getLogger(Client.class.getName()).log(Level.INFO, "Activity obtained for Url: {0}", url);
        } catch (Exception e) {
            Logger.getLogger(Client.class.getName()).log(Level.WARNING, "Exception in setActivity:{0}", e.getMessage());
            avo = new ActivityVO();
            avo.setCurrentRequests("null");
            avo.setFreeMemory("null");
            avo.setThreads("null");
            avo.setTotalMemory("null");
            activity = avo;
            serviceRegistrarList.stream().filter(s -> s.getUrl().equals(url)).forEach(svo -> svo.setBadConnection(true));
        }
    }

    /**
     * @return the subscriptions
     */
    public List<SubscriptionVO> getSubscriptions() {
        return subscriptions;
    }

    /**
     * @param subscriptions the subscriptions to set
     * @param currentService the currentService to set
     */
    public void setSubscriptionsAndCurrentService(List<SubscriptionVO> subscriptions, String currentService) {
        this.subscriptions = subscriptions;
        this.currentService = currentService;
    }

    /**
     * @return the svoToBeRegistered
     */
    public ServiceVO getSvoToBeRegistered() {
        return svoToBeRegistered;
    }

    /**
     * @return the registerIp
     */
    public String getRegisterIp() {
        return registerIp;
    }

    /**
     * @param registerIp the registerIp to set
     */
    public void setRegisterIp(String registerIp) {
        this.registerIp = registerIp;
    }

    /**
     * @return the registerPort
     */
    public String getRegisterPort() {
        return registerPort;
    }

    /**
     * @param registerPort the registerPort to set
     */
    public void setRegisterPort(String registerPort) {
        this.registerPort = registerPort;
    }
    
}
