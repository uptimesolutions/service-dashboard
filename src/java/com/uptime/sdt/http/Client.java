/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.sdt.http;

import com.uptime.sdt.vo.ActivityVO;
import com.uptime.sdt.vo.ServiceVO;
import com.uptime.sdt.vo.SubscriptionVO;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.sdt.vo.ServiceRegistrarVO;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class Client implements Serializable {
    private final int READ_TIMEOUT;
    private final int CONNECTION_TIMEOUT;
    private final String SHUTDOWN, REGISTER, UNREGISTER, SUBSCRIBE, UNSUBSCRIBE;

    /**
     * Constructor
     */
    public Client() {
        READ_TIMEOUT = 5000;
        CONNECTION_TIMEOUT = 5000;
        
        SHUTDOWN = "{\"serviceCommand\":\"shutdown\"}";
        REGISTER = "{\"serviceCommand\":\"register\"}";
        UNREGISTER = "{\"serviceCommand\":\"unregister\"}";
        SUBSCRIBE = "{\"serviceCommand\":\"subscribe\"}";
        UNSUBSCRIBE = "{\"serviceCommand\":\"unsubscribe\"}"; 
    }
    
    /**
     * Get a json of all service hosts and returns a list of ServiceRegistrarVO objects of the info received
     * @param srvoList, List of ServiceRegistrarVO objects
     * @return, list of ServiceRegistrar object 
     */
    public List<ServiceRegistrarVO> getServiveHosts(List<ServiceRegistrarVO> srvoList) {
        HttpURLConnection connection;
        JsonElement element;
        JsonObject json;
        JsonArray services, hosts;
        String content, name;
        ServiceVO svo;
        List<ServiceVO> svoList;
        
        for (ServiceRegistrarVO vo : srvoList) {
            if (getConnection(vo.getUrl())) { // check the connection of the serviceRegistrar
                vo.setBadConnection(false);
                try {
                    svoList = new ArrayList();
                    connection = (HttpURLConnection) new URL(vo.getServiceHostsUrl()).openConnection();
                    connection.setReadTimeout(READ_TIMEOUT);
                    connection.setConnectTimeout(CONNECTION_TIMEOUT);

                    if (connection.getResponseCode() == 200) {
                        try (InputStream response = connection.getInputStream();
                                Scanner scanner = new Scanner(response);) {
                            if ((content = scanner.next()) != null && (element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("serviceHosts")){
                                if ((services = json.getAsJsonArray("serviceHosts")) != null) {
                                    for (int i = 0; i < services.size(); i++) {
                                        if ((element = services.get(i)) != null && (json = element.getAsJsonObject()) != null && json.has("service") && json.has("hosts")){
                                            if ((name = json.get("service").getAsString()) != null && (hosts = json.getAsJsonArray("hosts")) != null) {
                                                for (int j = 0; j < hosts.size(); j++) {
                                                    if ((element = hosts.get(j)) != null && (json = element.getAsJsonObject()) != null && json.has("ipAddress") && json.has("port") && json.has("circuitBreaker")){
                                                        try {
                                                            svo = new ServiceVO();
                                                            svo.setName(name);
                                                            svo.setIpAddress(json.get("ipAddress").getAsString());
                                                            svo.setPort(json.get("port").getAsString());
                                                            svo.setCircuitBreaker(json.get("circuitBreaker").getAsBoolean());
                                                            svo.setCommand("");
                                                            svoList.add(svo);
                                                        }catch (Exception e){}
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } 
                } catch (Exception e) {
                    svoList = new ArrayList();
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, "Exception in getServiveHosts:{0}", e.getMessage());
                }
                vo.setSvoList(svoList);
            } else {
                vo.setSvoList(new ArrayList());
                vo.setBadConnection(true);
            }
        }
        return srvoList;
    }
    
    /**
     * Gets a json of subscriptions and populates the svoList files with in the given list of ServiceRegistrarVO objects 
     * @param srvoList, List of ServiceRegistrarVO objects
     * @return, list of ServiceRegistrar object 
     */
    public List<ServiceRegistrarVO> getSubscriptions(List<ServiceRegistrarVO> srvoList) {
        HttpURLConnection connection;
        JsonElement element;
        JsonObject json;
        JsonArray subscriptions, hosts;
        String content, name;
        
        for (ServiceRegistrarVO vo : srvoList) {
            // Get the json and adds all appropriate subscriptions 
            try {
                if(vo.getSvoList().size() > 0) {
                    connection = (HttpURLConnection) new URL(vo.getSubscriptionsUrl()).openConnection();
                    connection.setReadTimeout(READ_TIMEOUT);
                    connection.setConnectTimeout(CONNECTION_TIMEOUT);

                    vo.getSvoList().stream().forEach(svo->svo.setSubscriptions(new ArrayList()));
                    if (connection.getResponseCode() == 200) {
                        try (InputStream response = connection.getInputStream();
                                Scanner scanner = new Scanner(response);) {
                            if ((content = scanner.next()) != null){
                                //Logger.getLogger(Client.class.getName()).log(Level.SEVERE, "*************content:{0}", content);
                                if ((element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("subscriptions")){
                                    if ((subscriptions = json.getAsJsonArray("subscriptions")) != null) {
                                        for (int i = 0; i < subscriptions.size(); i++) {
                                            if ((element = subscriptions.get(i)) != null && (json = element.getAsJsonObject()) != null && json.has("service") && json.has("hosts")){
                                                if ((name = json.get("service").getAsString()) != null && (hosts = json.getAsJsonArray("hosts")) != null)
                                                    vo.setSvoList(getSubscriptionsHelper(name, vo.getSvoList(), hosts));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } 
                }
            } catch (Exception e) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, "Exception in getSubscriptions:{0}", e.getMessage());
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                if(vo.getSvoList().size() > 0)
                    vo.getSvoList().stream().forEach(svo->svo.setSubscriptions(new ArrayList()));
            }
        }
        // Check the connectivity of the hosts
        srvoList.stream().filter(registrar -> registrar.getSvoList() != null && !registrar.getSvoList().isEmpty()).forEachOrdered(registrar -> registrar.getSvoList().stream().forEachOrdered(service -> service.setConnection(getConnection(service.getIpAddress(), String.valueOf(service.getPort())))));
        
        return srvoList;
    }
    
    /**
     * Helping method for the getSubscriptions method
     * @param name, String object
     * @param svoList, list of ServiceVO object
     * @param hosts, JsonArray object
     * @return, list of ServiceVO object
     * @throws Exception 
     */
    private List<ServiceVO> getSubscriptionsHelper(String name, List<ServiceVO> svoList, JsonArray hosts) throws Exception {
        svoList.stream().filter(s -> s.getName().equalsIgnoreCase(name)).forEach(s -> {
            JsonElement element;
            JsonObject json;
            SubscriptionVO subscription;
            
            for (int j = 0; j < hosts.size(); j++) {
                if ((element = hosts.get(j)) != null && (json = element.getAsJsonObject()) != null && json.has("ipAddress") && json.has("port") && json.has("url") && json.has("source")){
                    try {
                        subscription = new SubscriptionVO();
                        subscription.setUrl(json.get("url").getAsString());
                        subscription.setIpAddress(json.get("ipAddress").getAsString());
                        subscription.setPort(json.get("port").getAsString());
                        subscription.setSource(json.get("source").getAsString());
                        s.getSubscriptions().add(subscription);
                    }catch (Exception e){}
                }
            }
        });
        return svoList;
    }
    
    /**
     * Gets a json of the Host activity of a given host and returns an VO of the info received
     * @param ipAddress, String object
     * @param port, String object
     * @return, ActivityVO object
     * @throws Exception 
     */
    public ActivityVO getActivity (String ipAddress, String port) throws Exception{
        HttpURLConnection connection;
        JsonElement element;
        String content;
        JsonObject json;
        ActivityVO avo;

        connection = (HttpURLConnection) new URL("http://" + ipAddress + ":" + port + "/system?serviceCommand=activity").openConnection();
        connection.setReadTimeout(READ_TIMEOUT);
        connection.setConnectTimeout(CONNECTION_TIMEOUT);

        if (connection.getResponseCode() == 200) {
            try (InputStream response = connection.getInputStream();
                    Scanner scanner = new Scanner(response);) {
                content = scanner.next();
                element = JsonParser.parseString(content);
                json = element.getAsJsonObject();
                
                avo = new ActivityVO();
                avo.setCurrentRequests(json.get("currentRequests").getAsString());
                avo.setTotalMemory(json.get("totalMemory").getAsString());
                avo.setFreeMemory(json.get("freeMemory").getAsString());
                avo.setThreads(json.get("threads").getAsString());
                return avo;
            } 
        } else throw new Exception("Error occured, status code: " + String.valueOf(connection.getResponseCode()));
    }
    
    /**
     * Gets a json of the given url's and returns an VO of the info received
     * @param url, String object
     * @return, ActivityVO object
     * @throws Exception 
     */
    public ActivityVO getActivity (String url) throws Exception{
        HttpURLConnection connection;
        JsonElement element;
        String content;
        JsonObject json;
        ActivityVO avo;

        connection = (HttpURLConnection) new URL(url + "/system?serviceCommand=activity").openConnection();
        connection.setReadTimeout(READ_TIMEOUT);
        connection.setConnectTimeout(CONNECTION_TIMEOUT);

        if (connection.getResponseCode() == 200) {
            try (InputStream response = connection.getInputStream();
                    Scanner scanner = new Scanner(response);) {
                content = scanner.next();
                element = JsonParser.parseString(content);
                json = element.getAsJsonObject();
                
                avo = new ActivityVO();
                avo.setCurrentRequests(json.get("currentRequests").getAsString());
                avo.setTotalMemory(json.get("totalMemory").getAsString());
                avo.setFreeMemory(json.get("freeMemory").getAsString());
                avo.setThreads(json.get("threads").getAsString());
                return avo;
            } 
        } else throw new Exception("Error occured, status code: " + String.valueOf(connection.getResponseCode()));
    }

    /**
     * Checks if the given info is a service and if so returns a ServiceVO
     * @param ipAddress, String object
     * @param port, String object
     * @return ServiceVO object
     * @throws Exception 
     */
    public ServiceVO getServiceInfo (String ipAddress, String port) throws Exception {
        HttpURLConnection connection;
        JsonElement element;
        String content;
        JsonObject json;
        ServiceVO svo;
                
        connection = (HttpURLConnection) new URL("http://" + ipAddress + ":" + port + "/system").openConnection();
        connection.setReadTimeout(READ_TIMEOUT);
        connection.setConnectTimeout(CONNECTION_TIMEOUT);

        if (connection.getResponseCode() == 200) {
            try (InputStream response = connection.getInputStream();
                    Scanner scanner = new Scanner(response);) {
                content = scanner.next();
                element = JsonParser.parseString(content);
                json = element.getAsJsonObject();
                
                svo = new ServiceVO();
                svo.setName(json.get("name").getAsString());
                svo.setIpAddress(json.get("ipAddress").getAsString());
                svo.setPort(json.get("port").getAsString());
                svo.setCommand("register");
                return svo;
            } 
        } else throw new Exception("Error occured, status code: " + String.valueOf(connection.getResponseCode()));
    }
    
    /**
     * Checks connectivity of the given service
     * @param ipAddress, String object
     * @param port, String object
     * @return, String object, "" if 200 is received, else "!"
     */
    private String getConnection (String ipAddress, String port) {
        HttpURLConnection connection;
                
        try {
            connection = (HttpURLConnection) new URL("http://" + ipAddress + ":" + port + "/system").openConnection();
            connection.setReadTimeout(2000);
            connection.setConnectTimeout(2000);
            if (connection.getResponseCode() == 200)
                return "";
        } catch (Exception ex) {}
        return "!";
    }
    
    /**
     * Checks connectivity of the given service
     * @param url, String object
     * @return, boolean, true if connected, else false
     */
    private boolean getConnection (String url) {
        HttpURLConnection connection;
                
        try {
            connection = (HttpURLConnection) new URL(url + "/system").openConnection();
            connection.setReadTimeout(2000);
            connection.setConnectTimeout(2000);
            return connection.getResponseCode() == 200;
        } catch (Exception ex) {
            return false;
        }
    }
    
    /**
     * Post a system command to the given service host
     * @param svo, ServiceVO object
     * @throws Exception 
     */
    public void postSystemCommand (ServiceVO svo) throws Exception {
        if(svo.getCommand() != null){
            switch (svo.getCommand().toLowerCase()){
                case "shutdown":
                    postSystemCommandHelper(svo, SHUTDOWN);
                    break;
                case "register":
                    postSystemCommandHelper(svo, REGISTER);
                    break;
                case "unregister":
                    postSystemCommandHelper(svo, UNREGISTER);
                    break;
                case "subscribe":
                    postSystemCommandHelper(svo, SUBSCRIBE);
                    break;
                case "unsubscribe":
                    postSystemCommandHelper(svo, UNSUBSCRIBE);
                    break;
                default:
                    throw new Exception("Error, \"" + svo.getCommand().toLowerCase() + "\" not an acceptable command!");
            }
        } else throw new Exception("Error, command is null");
    }
    
    /**
     * Post a system command to the given ServiceRegistrar
     * @param srvo, ServiceRegistrarVO object
     * @throws Exception 
     */
    public void postSystemCommand (ServiceRegistrarVO srvo) throws Exception {
        if(srvo.getCommand() != null){
            switch (srvo.getCommand().toLowerCase()){
                case "shutdown":
                    postSystemCommandHelper(srvo, SHUTDOWN);
                    break;
                default:
                    throw new Exception("Error, \"" + srvo.getCommand().toLowerCase() + "\" not an acceptable command!");
            }
        } else throw new Exception("Error, command is null");
    }
    
    /**
     * Helping method for the postSystemCommand method
     * @param svo, ServiceVO object
     * @param json, String object, of the json command
     * @throws Exception 
     */
    private void postSystemCommandHelper(ServiceVO svo, String json) throws Exception {
        HttpURLConnection connection;
        int code;
        
        connection = (HttpURLConnection) new URL("http://" + svo.getIpAddress() + ":" + svo.getPort() + "/system").openConnection();
        connection.setReadTimeout(READ_TIMEOUT);
        connection.setConnectTimeout(CONNECTION_TIMEOUT);
        connection.setRequestMethod("POST");
        connection.setDoInput(true);
        connection.setDoOutput(true);
        
        // Post Json
        try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"))){
            writer.write(json);
        }
        
        // Check response Status code
        if ((code = connection.getResponseCode()) != 200)
            throw new Exception("Error, Status Code:" + code);
    }
    
    /**
     * Helping method for the postSystemCommand method
     * @param srvo, ServiceRegistrarVO object
     * @param json, String object, of the json command
     * @throws Exception 
     */
    private void postSystemCommandHelper(ServiceRegistrarVO srvo, String json) throws Exception {
        HttpURLConnection connection;
        int code;
        
        connection = (HttpURLConnection) new URL(srvo.getUrl() + "/system").openConnection();
        connection.setReadTimeout(READ_TIMEOUT);
        connection.setConnectTimeout(CONNECTION_TIMEOUT);
        connection.setRequestMethod("POST");
        connection.setDoInput(true);
        connection.setDoOutput(true);
        
        // Post Json
        try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"))){
            writer.write(json);
        }
        
        // Check response Status code
        if ((code = connection.getResponseCode()) != 200)
            throw new Exception("Error, Status Code:" + code);
    }
    
    /**
     * unsubscribe the given subscription from the given services from the list of serviceRegistrarVO objects
     * @param srvoList, List of ServiceRegistrarVO objects
     * @param service, String objects
     * @param svo, SubscriptionVO object
     * @throws Exception 
     */
    public void putUnsubscribe (List<ServiceRegistrarVO> srvoList, String service, SubscriptionVO svo) throws Exception{
        HttpURLConnection connection;
        StringBuilder json;
        int code;
        
        json = new StringBuilder();
        json
            .append("{\"names\":[{\"name\":\"").append(service).append("\"}],")
            .append("\"ipAddress\":\"").append(svo.getIpAddress() != null ? svo.getIpAddress() : "").append("\",")
            .append("\"url\":\"").append(svo.getUrl() != null ? svo.getUrl() : "").append("\",")
            .append("\"port\":\"").append(svo.getPort() != null ? svo.getPort() : "0").append("\",")
            .append("\"source\":\"").append(svo.getSource()).append("\"}");
        
        for (ServiceRegistrarVO vo : srvoList) {
            connection = (HttpURLConnection) new URL(vo.getUrl() + "/subscribe").openConnection();
            connection.setReadTimeout(READ_TIMEOUT);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);
            connection.setRequestMethod("PUT");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            
            // Post Json
            try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"))){
                writer.write(json.toString());
            }

            // Check response Status code
            if ((code = connection.getResponseCode()) != 200)
                throw new Exception("Error, Status Code:" + code);
        }
    }
}
