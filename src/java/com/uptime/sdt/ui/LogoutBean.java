/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.sdt.ui;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author twilcox
 */
public class LogoutBean implements Serializable {
    
    /**
     * Used to logout a user from his current session
     * @return String for redirecting
     */
    public String logout() {
        String result = "/login.xhtml?faces-redirect=true";

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
        try {
            request.logout();
            request.getSession().invalidate();
        } catch (ServletException e) {
            Logger.getLogger(LogoutBean.class.getName()).log(Level.SEVERE, "Failed to logout user!", e);
            result="/loginError.xhtml?faces-redirect=true";
        }
        return result;
    }
}
