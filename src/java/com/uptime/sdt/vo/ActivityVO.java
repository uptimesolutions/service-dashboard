/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.sdt.vo;

import java.io.Serializable;

/**
 *
 * @author twilcox
 */
public class ActivityVO implements Serializable {
    private String currentRequests;
    private String totalMemory;
    private String freeMemory;
    private String threads;

    /**
     * @return the currentRequests
     */
    public String getCurrentRequests() {
        return currentRequests;
    }

    /**
     * @param currentRequests the currentRequests to set
     */
    public void setCurrentRequests(String currentRequests) {
        this.currentRequests = currentRequests;
    }

    /**
     * @return the totalMemory
     */
    public String getTotalMemory() {
        return totalMemory;
    }

    /**
     * @param totalMemory the totalMemory to set
     */
    public void setTotalMemory(String totalMemory) {
        this.totalMemory = totalMemory;
    }

    /**
     * @return the freeMemory
     */
    public String getFreeMemory() {
        return freeMemory;
    }

    /**
     * @param freeMemory the freeMemory to set
     */
    public void setFreeMemory(String freeMemory) {
        this.freeMemory = freeMemory;
    }

    /**
     * @return the threads
     */
    public String getThreads() {
        return threads;
    }

    /**
     * @param threads the threads to set
     */
    public void setThreads(String threads) {
        this.threads = threads;
    }
}
