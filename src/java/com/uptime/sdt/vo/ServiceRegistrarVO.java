/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.sdt.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author twilcox
 */
public class ServiceRegistrarVO implements Serializable {
    private String url;
    private String serviceHostsUrl;
    private String subscriptionsUrl;
    private List<ServiceVO> svoList;
    private boolean badConnection;
    private String command;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getServiceHostsUrl() {
        return serviceHostsUrl;
    }

    public void setServiceHostsUrl(String serviceHostsUrl) {
        this.serviceHostsUrl = serviceHostsUrl;
    }

    public String getSubscriptionsUrl() {
        return subscriptionsUrl;
    }

    public void setSubscriptionsUrl(String subscriptionsUrl) {
        this.subscriptionsUrl = subscriptionsUrl;
    }

    public List<ServiceVO> getSvoList() {
        return svoList;
    }

    public void setSvoList(List<ServiceVO> svoList) {
        this.svoList = svoList;
    }

    public boolean isBadConnection() {
        return badConnection;
    }

    public void setBadConnection(boolean badConnection) {
        this.badConnection = badConnection;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}
