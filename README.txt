Configure Tomcat server to use Sugar:
1)	Your tomcat server needs to be configured to communicate with Sugar.
2)	If the tomcat server is not configured to use Sugar you can replace the server.xml file in your tomcat conf folder, with the server.xml file found with this application in the tomcat folder.

First time starting SDT:
1)	Create a text file and name it ServiceRegistrarUrls.txt.
2)	Put the newly created text file in the webapps folder on your tomcat server.

To Add a new ServiceRegistrar:
1)	Open the ServiceRegistrarUrls.txt file in the webapps folder.
2)	Add the the ServiceRegistrar host URL on the first available empty line.
		A)	Only one host URL can be on each line.
		B)	Example of host URLs:
				1)	http://services.abnetwork.com
				2)	http://localhost:8081
3) 	Save and close the ServiceRegistrarUrls.txt file.
4)	The service doesn't need to be shutdown to update the file because the service auto refreshes ever 60 seconds.
 